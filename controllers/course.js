const Course = require("../models/Course");

//Add Course-Admin Only
//Activity Solution 1

/*module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({	
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}
*/

//Activity Solution 2


module.exports.addCourse = async (user, reqBody) => {
	if(user.isAdmin == true){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return ("Not Authorized");
	}
}

//Retrieve all the courses
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});
};

//Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

//Retrieve specific course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		let course = {
			name: result.name,
			price:result.price
		}
		return course

	});
};


//Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	//Syntax:
		//findByIdAndUpdate(document, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then((course, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
}

// Activity - Nov 4

module.exports.archiveCourse = (reqParams, reqBody) => {

	let archivedCourse = {
		isActive: reqBody.isActive
	}

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse)
	.then((course, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
}


