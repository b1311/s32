const express = require("express");
const router = express.Router();
const userController = require("../controllers/user")
const auth = require("../auth");

//Route for checking duplicate email
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController =>
		res.send(resultFromController))
})

//Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController =>
		res.send(resultFromController))

})

//Route for User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController =>
		res.send(resultFromController))
})

//Route for Retrieving User Details

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	userController.getProfile({userId: req.body.id}).then(resultFromController => 
		res.send(resultFromController))
})






//Activity (Get Profile)
// router.post("/details", (req, res) => {
// 	userController.getProfile(req.body).then(resultFromController =>
// 		res.send(resultFromController))
// })

//Route for Retrieving User Details - solution 2
// router.get("/details", auth.verify, (req, res) => {
// 	const userData = auth.decode(req.headers.authorization)
	
// 	userController.getProfile({userId: req.body.id}).then(resultFromController => 
// 		res.send(resultFromController))
// })

//For enrolling a user
// router.post("/enroll", (req, res) => {

// 	let data = {
// 		userId: req.body.userId,
// 		courseId: req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController => 
// 		res.send(resultFromController));

// });

//Activity enroll

router.post("/enrollroute", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => 
		res.send(resultFromController));

});



module.exports = router;