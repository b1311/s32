const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

//Add Course-Admin Only
//Activity Solution 1

/*router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		courseController.addCourse(req.body).then(resultFromController => 
		res.send(resultFromController))
	} else {
		res.send("Not Authorized")
	}	
})
*/

//Activity Solution 2


router.post("/", auth.verify, (req, res) => {		
	const userData = auth.decode(req.headers.authorization);

	courseController.addCourse(userData, req.body).then(resultFromController => 
		res.send(resultFromController))
});


//Retrieve all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => 
		res.send(resultFromController))
});


//Retrive all active course
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => 
		res.send(resultFromController));
});

//Retrieve specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params)
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => 
		res.send(resultFromController))
});

//Update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})


// Activity - Nov 4

router.put("/:courseId/archive", auth.verify, (req, res) => {
	console.log(req.params)
	console.log(req.params.courseId)
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// Activity solution 2:
// Route for archiving a course
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases


// router.put("/:courseId/archive", auth.verify, (req, res) => {

// 	const user = auth.decode(req.headers.authorization)
// 	courseController.archiveCourse(user, req.params, req.body).then(resultFromController => res.send(resultFromController));	
// });




module.exports = router;
